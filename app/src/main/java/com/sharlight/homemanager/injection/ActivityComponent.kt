package com.sharlight.homemanager.injection

import com.sharlight.homemanager.injection.module.ActivityModule
import com.sharlight.homemanager.ui.dashboard.MainActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)
}