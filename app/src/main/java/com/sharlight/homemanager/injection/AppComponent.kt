package com.sharlight.homemanager.injection

import com.sharlight.homemanager.injection.module.NetworkModule
import com.sharlight.homemanager.injection.module.AppModule
import com.sharlight.homemanager.injection.module.DataModule
import com.sharlight.homemanager.injection.module.PresenterModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, DataModule::class, NetworkModule::class))
interface AppComponent {
    operator fun plus(presenterModule: PresenterModule): ConfigPersistentComponent
}

