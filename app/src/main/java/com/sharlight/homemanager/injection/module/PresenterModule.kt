package com.sharlight.homemanager.injection.module

import com.sharlight.homemanager.injection.ConfigPersistent
import com.sharlight.homemanager.ui.dashboard.DashboardContract
import com.sharlight.homemanager.ui.dashboard.DashboardPresenter
import dagger.Module
import dagger.Provides

@Module
class PresenterModule {

    @ConfigPersistent
    @Provides
    fun providesMainPresenter(): DashboardContract.Presenter = DashboardPresenter()
}
