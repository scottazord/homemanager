package com.sharlight.homemanager.injection.module

import android.support.v7.app.AppCompatActivity
import dagger.Module

@Module
class ActivityModule(private val activity: AppCompatActivity)
