package com.sharlight.homemanager.ui.dashboard



class DashboardPresenter : DashboardContract.Presenter {

    private var view: DashboardContract.View? = null

    override fun attachView(view: DashboardContract.View) {
       this.view = view
    }

    override fun detachView() {
        this.view = null
    }


    override fun buttonClicked() {
        view?.showMessage("Button was clicked")
    }


}

