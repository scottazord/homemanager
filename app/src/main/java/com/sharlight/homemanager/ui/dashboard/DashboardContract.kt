package com.sharlight.homemanager.ui.dashboard

import com.sharlight.homemanager.ui.BasePresenter
import com.sharlight.homemanager.ui.BaseView


interface DashboardContract {
    interface View : BaseView {
        fun showMessage( message: String )
    }

    interface Presenter : BasePresenter<View> {
        fun buttonClicked()
    }
}