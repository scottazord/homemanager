package com.sharlight.homemanager.ui

interface BasePresenter<in V : BaseView > {

    fun attachView(view: V)
    fun detachView()

}

