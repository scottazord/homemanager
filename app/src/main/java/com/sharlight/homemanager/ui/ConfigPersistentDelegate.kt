package com.sharlight.homemanager.ui

import android.content.Context
import android.os.Bundle
import com.sharlight.homemanager.extensions.appComponent
import com.sharlight.homemanager.injection.ConfigPersistentComponent
import com.sharlight.homemanager.injection.module.PresenterModule
import java.util.concurrent.atomic.AtomicLong


class ConfigPersistentDelegate {

    companion object {
        private const val KEY_ID = "KEY_ID"
        @JvmStatic private val NEXT_ID = AtomicLong(0)
        @JvmStatic private val componentsMap = HashMap<Long, ConfigPersistentComponent>()
    }

    private var id: Long = 0

    var instanceSaved = false
    lateinit var component: ConfigPersistentComponent
        get

    fun onCreate(context: Context, savedInstanceState: Bundle?) {
        // Create the ActivityComponent and reuses cached ConfigPersistentComponent if this is
        // being called after a configuration change.
        id = savedInstanceState?.getLong(KEY_ID) ?: NEXT_ID.getAndIncrement()
        component = componentsMap.getOrPut(id, { context.appComponent + PresenterModule()  })
    }

    fun onSaveInstanceState(outState: Bundle) {
        outState.putLong(KEY_ID, id)
        instanceSaved = true
    }

    fun onDestroy() {
        if (!instanceSaved) {
            componentsMap.remove(id)
        }
    }
}