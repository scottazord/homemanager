package com.sharlight.homemanager.ui.dashboard

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.ViewGroup
import android.widget.Button
import com.sharlight.homemanager.R
import com.sharlight.homemanager.extensions.bind
import com.sharlight.homemanager.ui.BaseActivity
import javax.inject.Inject

class MainActivity : BaseActivity(), DashboardContract.View {

    private val button by bind<Button>(R.id.button)
    private val rootView by bind<ViewGroup>(android.R.id.content)

    @Inject
    lateinit var presenter: DashboardContract.Presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activityComponent.inject(this)
        presenter.attachView(this)
        button.setOnClickListener { presenter.buttonClicked() }
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }


    override fun showMessage(message: String) {
        showSnack(rootView, message, Snackbar.LENGTH_INDEFINITE, "Try Again", {}, {})
    }


}
