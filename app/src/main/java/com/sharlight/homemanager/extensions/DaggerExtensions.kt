package com.sharlight.homemanager.extensions

import android.content.Context
import com.sharlight.homemanager.AppApplication

val Context.appComponent
    get() = (applicationContext as AppApplication).appComponent